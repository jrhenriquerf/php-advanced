<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Validator;


class ApiController extends Controller {
    public function products() {
        $products = Product::with(['categories', 'images'])->get();
        return response()->json($products);
    }

    public function categories() {
        $categories = Category::get();
        return response()->json($categories);
    }

    /**
     * save a new category
     *
     * @param \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function saveCategories(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:10',
            'url_image' => 'required'
        ]);

        $name = $request->input("name");
        $url = $request->input("url_image");

        if (!$validator->fails()) {

            $contents = file_get_contents($url);
            $namefile = time() . '.' . substr($url, strpos($url, '.') + 1);
            Storage::disk("public")->put($namefile, $contents);

            if (empty($category)) {
                $category = Category::create([
                    'name' => $name,
                    'image' => $namefile
                ]);
                
                if (!empty($category)) {
                    return response()->json($category, 201);
                }
            }

            return response()->json([
                "message" => "Erro ao salvar categoria"
            ], 500);
        }
        
    }
}