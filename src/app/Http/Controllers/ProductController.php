<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use DB;
use Validator;

class ProductController extends Controller
{
    private $path = 'images/product';

    public function __construct() 
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        $categories = Category::get();
        $selectedCat = [];

        return view('product.index', compact('products', 'categories', 'selectedCat'));
    }

    /**
     * Search for a product.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();

        return view('product.add', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $name = $request->input('name');
        $selectedCat = $request->input('category');

        $query = DB::table('products')
                    ->select('products.id', 'products.name', 'products.description')
                    ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
                    ->join('categories', 'product_categories.category_id', '=', 'categories.id')
                    ->groupBy('products.id', 'products.name', 'products.description');

        if (!empty($name) && !empty($selectedCat)) {
            $query->where('products.name', 'like', '%' . $name . '%');
            $query->whereIn('categories.id', $selectedCat);
        } else if (!empty($name)) {
            $query->where('products.name', 'like', '%' . $name . '%');
        } else if (!empty($selectedCat)) {
            $query->whereIn('categories.id', $selectedCat);
        }

        $categories = Category::get();
        $products = $query->get();

        if (empty($selectedCat)) {
            $selectedCat = [];
        }

        return view('product.index', compact('products', 'categories', 'selectedCat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:10|max:255',
            'description' => 'required',
        ]);

        if (!$validator->fails()) {
            $images = $request->file('images');

            $product = Product::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
            ]);

            if (!empty($product) && !empty($images)) {
                foreach ($images as $key => $row) {
                    if (!empty($row)) {
                        $fileName = time() . $key . '.' . $row->getClientOriginalExtension();
                        $row->move($this->path, $fileName);
                        $image = new ProductImage;
                        $image->product_id = $product->id;
                        $image->image = $fileName;
                        $image->save();
                    }
                }
            }

            $product->categories()->sync($request->input('category'));
        }

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::find($product->id);

        if (!empty($product)) {
            $categories = Category::get();
            $images = ProductImage::where('product_id', $product->id)->get();
            $selectedCat = [];

            foreach ($product->categories as $category) {
                $selectedCat[] = $category->pivot->category_id;
            }

            return view(
                'product.edit', 
                compact('product', 'categories', 'selectedCat', 'images')
            );
        }

        return redirect()->route('product.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $images = $request->file('images');
        $category = $request->input('category');

        $product = Product::find($product->id);

        if (!empty($product)) {
            if (!empty($images)) {
                foreach($images as $key => $row) {
                    if (!empty($row)) {
                        $fileName = time() . $key . '.' . $row->getClientOriginalExtension();
                        $row->move($this->path, $fileName);
                        $image = new ProductImage;
                        $image->product_id = $product->id;
                        $image->image = $fileName;
                        $image->save();
                    }
                }
            }

            if (!empty($category)) {
                $product->categories()->sync($category);
            }

            $product->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
            ]);

            return redirect()->route('product.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product = Product::find($product->id);

        if (!empty($product)) {
            $images = ProductImage::where('product_id', $product->id)->get();
            
            if (!empty($images)) {
                foreach ($images as $row) {
                    if(file_exists($this->path . '/' . $row->image)) {
                        unlink($this->path . '/' . $row->image);
                    }
                }
            }

            $product->categories()->detach();
            $product->images()->delete();
            $result = $product->delete();
        }

        return redirect()->route('product.index');
    }
}
