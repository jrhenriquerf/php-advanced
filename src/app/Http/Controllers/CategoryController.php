<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $path = 'images/category';

    public function __construct() 
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(10);
        return view('category.index', compact('categories'));
    }

    /**
     * Search for a category.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $name = $request->input('name');
        $search = true;

        if (!$name) {
            return redirect()->route('category.index');
        }

        $categories = Category::where('name', 'like', "%$name%")->get();

        return view('category.index', compact('categories', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!empty($request->file('image')) && $request->file('image')->isValid()) {
            $fileName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move($this->path, $fileName);
        }

        $result = Category::create([
            'name' => $request->input('name'),
            'image' => $fileName
        ]);

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categoryData = Category::find($category);

        if (!$categoryData) {
            return redirect()->route('category.index');
        }

        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $fileName = null;

        if (!empty($request->file('image')) && $request->file('image')->isValid()) {
            $imagePath = $this->path . '/' . $request->input('deleteimage');

            if (!empty($request->input('deleteimage')) && file_exists($imagePath)) {
                unlink($imagePath);
            }

            $fileName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move($this->path, $fileName);
        }
        
        $update = [
            'name' => $request->input('name'),
        ];

        if ($fileName) {
            $update = [
                'name' => $request->input('name'),
                'image' => $fileName
            ];
        }
        
        $result = Category::find($category->id)->update($update);

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $categoryData = Category::find($category->id);
        
        if ($categoryData) {
            $categoryData->products()->detach(); // Deleta os relacionamentos dos produtos relacionados a esta categoria
            $result = $category->delete(); // Deleta a categoria
        }

        return redirect()->route('category.index');
    }
}
