<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description'];

    protected $dates = ['deleted_at'];

    public function categories() {
        return $this->belongsToMany('App\Models\Category', 'product_categories');
    }

    public function images() {
        return $this->hasMany('App\Models\ProductImage');
    }
}
